import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 * @since 1.8
 */
public class GraphTask {

    /** Main method. */
    public static void main (String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    private static int NUM_VERTEXES = 9;
    private static int NUM_EDGES = 8;

    /** Actual main method to run examples and everything. */
    public void run() {
        Graph g = new Graph ("G");
        g.createRandomSimpleGraph (NUM_VERTEXES, NUM_EDGES);
        System.out.println (g);

        Vertex t = chooseRandomVertex(g);
        Vertex s =  g.first;
        g.findLongestPath(s,t);
    }

    private Vertex chooseRandomVertex(Graph g) {
        Random random = new Random();
        int idx = random.nextInt(NUM_VERTEXES + 1);
        Vertex chosen = g.first;
        while (idx > 0 && chosen.next != null) {
            chosen = chosen.next;
            idx--;
        }
        return chosen;
    }

    /**
     * PROBLEM DESCRIPTION
     *
     * C-13.19
     * Design an efficient algorithm for finding a longest directed path from a vertex S
     * to a vertex T of an acyclic weighted digraph.
     *
     * */

    class Vertex {

        private String id;
        private Vertex next;
        /**
         * For some reason our vertexes don't actually know about their edges/vertices.
         * We have to check the vertices and see if each knows about any other vertices.
         * This increases our time complexity.
         */
        private Edge first;
        private int info = 0;

        Vertex (String s, Vertex v, Edge e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex (String s) {
            this (s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Vertex getNext() {
            return next;
        }

        public void setNext(Vertex next) {
            this.next = next;
        }

        public Edge getFirst() {
            return first;
        }

        public void setFirst(Edge first) {
            this.first = first;
        }

        public int getVInfo() {
            return info;
        }

        public void setVInfo(int info) {
            this.info = info;
        }

        public Iterator<Edge> outEdges() {
            List<Edge> expensiveListToCompensateForRidiculousArchitecture = new ArrayList<>();
            expensiveListToCompensateForRidiculousArchitecture.add(first);
            Edge e = first;
            while (e.next != null) {
                expensiveListToCompensateForRidiculousArchitecture.add(e.next);
                e = e.next;
            }
            return expensiveListToCompensateForRidiculousArchitecture.iterator();
        }

    }


    /** Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Edge {

        private String id;
        private Vertex target;
        private Edge next;
        private int info = 0;

        Edge(String s, Vertex v, Edge a) {
            id = s;
            target = v;
            next = a;
        }

        Edge(String s) {
            this (s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        public Vertex getTarget() {
            return target;
        }

    }


    /** This header represents a graph.
     */
    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;
        int longestPath;
        private Vertex[] allVertices;
        private List<Edge> allEdges;
        private int[][] adjMatrix;

        Graph (String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph (String s) {
            this (s, null);
        }

        /**
         * Based on example from https://enos.itcollege.ee/~japoia/algorithms/graphs1.html
         *
         * Topological sort of vertices.
         * For each vertex vInfo=0 if the graph cannot be sorted,
         * otherwise vInfo is the ordinal number of the vertex.
         */
        public void topolSort() {
            boolean cycleFound = false;
            setGInfo (0); // count number of vertices
            Iterator<Vertex> vit = vertices();
            while (vit.hasNext()) {
                vit.next().setVInfo (0);
                setGInfo (info + 1);
            }
            Iterator<Edge> eit = edges();
            while (eit.hasNext()) {
                Edge e = eit.next();
                Vertex v = e.getTarget();
                // count number of incoming edges
                v.setVInfo (v.getVInfo() + 1);
            }
            List<Vertex> start = Collections.synchronizedList(new LinkedList<>());
            vit = vertices();
            while (vit.hasNext()) {
                Vertex v = vit.next();
                if (v.getVInfo() == 0) {
                    start.add(v); // no incoming edges
                }
            }

            List<Vertex> order = Collections.synchronizedList(new LinkedList<>());
            if (start.size() == 0) cycleFound = true;
            while ((!cycleFound)&(start.size() != 0)) {
                Vertex current = start.remove (0); // first vertex
                order.add (current);
                eit = current.outEdges(); // "remove" outgoing edges
                while (eit.hasNext()) {
                    Edge e = eit.next();
                    Vertex v = e.getTarget();
                    v.setVInfo (v.getVInfo() - 1);
                    if (v.getVInfo() == 0) {
                        start.add (v); // no incoming edges anymore
                    }
                }
            }
            if (info != order.size()) cycleFound = true;
            if (cycleFound) {
                Iterator<Vertex> it = vertices();
                while (it.hasNext()) {
                    Vertex v = it.next();
                    v.setVInfo (0);
                }
            } else {
                int i = 0;
                for (Vertex vertex : order) {
                    i++;
                    vertex.setVInfo(i);
                }
            }
            setGInfo (0);
        }

        private Iterator<Vertex> vertices() {
            return new Iterator<Vertex>() {

                private int index = 0;

                @Override
                public boolean hasNext() {
                    return allVertices.length > index;
                }

                @Override
                public Vertex next() {
                    return allVertices[index++];
                }

            };
        }

        private Iterator<Edge> edges() {
            return new Iterator<Edge>() {

                private int index = 0;

                @Override
                public boolean hasNext() {
                    return allEdges.size() > index;
                }

                @Override
                public Edge next() {
                    return allEdges.get(index++);
                }

            };
        }

        private void setGInfo(int i) {
            info = i;
        }

        @Override
        public String toString() {
            String nl = System.getProperty ("line.separator");
            StringBuffer sb = new StringBuffer (nl);
            sb.append (id);
            sb.append (nl);
            Vertex v = first;
            while (v != null) {
                sb.append (v.toString());
                sb.append (" [vInfo = ");
                sb.append (v.getVInfo());
                sb.append ("]");
                sb.append (" -->");
                Edge a = v.first;
                while (a != null) {
                    sb.append (" ");
                    sb.append (a.toString());
                    sb.append (" (");
                    sb.append (v.toString());
                    sb.append ("->");
                    sb.append (a.target.toString());
                    sb.append (")");
                    a = a.next;
                }
                sb.append (nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex (String vid) {
            Vertex res = new Vertex (vid);
            res.next = first;
            first = res;
            return res;
        }

        public Edge createEdge(String aid, Vertex from, Vertex to) {
            Edge res = new Edge(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         * @param n number of vertices added to this graph
         */
        public void createRandomTree (int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex [n];
            for (int i = 0; i < n; i++) {
                varray [i] = createVertex ("v" + String.valueOf(n-i));
                if (i > 0) {
                    int vnr = (int)(Math.random()*i);
                    //NOTE: prof isnt creating weights?
                    createEdge("a" + varray [vnr].toString() + "_"
                            + varray [i].toString(), varray [vnr], varray [i]);
                    createEdge("a" + varray [i].toString() + "_"
                            + varray [vnr].toString(), varray [i], varray [vnr]);
                } else {}
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int [info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Edge a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res [i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph (int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException ("Too many vertices: " + n);
            if (m < n-1 || m > n*(n-1)/2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree (n);       // n-1 edges created here
            allVertices = new Vertex [n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                allVertices[c++] = v;
                v = v.next;
            }
            adjMatrix = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            allEdges = new ArrayList<>();
            while (edgeCount > 0) {
                int i = (int)(Math.random()*n);  // random source
                int j = (int)(Math.random()*n);  // random target
                if (i==j)
                    continue;  // no loops
                if (adjMatrix [i][j] != 0 || adjMatrix [j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = allVertices[i];
                Vertex vj = allVertices[j];
                allEdges.add(createEdge("a" + vi.toString() + "_" + vj.toString(), vi, vj));
                adjMatrix [i][j] = 1;
                allEdges.add(createEdge("a" + vj.toString() + "_" + vi.toString(), vj, vi));
                adjMatrix [j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * Example taken from https://www.geeksforgeeks.org/shortest-path-for-directed-acyclic-graphs/
         * @param startVertex
         * @param targetVertex
         */
        private void findLongestPath(Vertex startVertex, Vertex targetVertex) {
            System.out.println(toString());
            topolSort();
            System.out.println(toString());
            longestPath = Integer.MIN_VALUE;
            HashMap<Integer, Integer> longestPaths = new HashMap<>();
            for (int idx=0; idx < adjMatrix.length; idx++) {
                if (longestPaths.get(idx) == null) {
                    longestPaths.put(idx, getMaxLength(longestPaths, idx));
                }
                longestPath = Math.max(longestPath, longestPaths.get(idx));
            }
            System.out.println("Longest Path = " + longestPath);
        }

        private int getMaxLength(HashMap<Integer, Integer> longestPaths, int idx) {
            Integer len = longestPaths.get(idx);
            if (len != null && len != 0) {
                return len;
            }
            System.out.println("getMaxLength running calc for the vertex with info " + idx);
            len = 1; //1 is our own weight, cen be zero once we count edge weights correctly
            longestPaths.put(idx, len);
            //all values in adjMatrix are vertex ordinals (for some reason called infos)
            if (idx < adjMatrix.length) {
                System.out.println("Checking " + adjMatrix[idx].length + " neighbours of " + idx);
                for (int neighborIdx : adjMatrix[idx]) {
                    if (neighborIdx != idx) {
                        //we are only counting vertex weight here, need to count edge weight
                        int neighbourWeight = 1 + getEdgeWeight(idx, neighborIdx) + getMaxLength(longestPaths, neighborIdx);
                        //System.out.println("Weight of neigh " + neighborIdx + " of " + idx + " is " + neighbourWeight);
                        len = Math.max(len, neighbourWeight);
                    }
                }
            }
            System.out.println("Longest path for " + idx + " is now " + len);
            longestPaths.put(idx, len);
            return len;
        }

        /**
         * Params are ordinals (infos) of vertexes
         */
        private int getEdgeWeight(int idx, int neighborIdx) {
            String check = "av" + idx + "_v" + neighborIdx;
            for (Edge e : allEdges) {
                //System.out.println("check " + e.id + " vs " + check);
                if (e.id.equals(check)) {
                    //System.out.println("returning edge " + e.id + " weight " + e.info);
                    return e.info;
                }
            }
            return 0;
        }

    }
} 

